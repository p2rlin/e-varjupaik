import json
import os

import cv2
import numpy as np
from skimage import img_as_ubyte
from skimage.exposure import equalize_adapthist
from skimage.feature import greycomatrix, greycoprops


def extract_color_moments(image):
    color_moments_fv = []
    image_blocks = blocks(image)
    for b in image_blocks:
        cms = color_moments(b)
        for cm in cms:
            color_moments_fv.append(cm)
    return fv2str(color_moments_fv)


def blocks(img):
    h = img.shape[0]
    w = img.shape[1]
    fh4 = int(h / 4)
    fw4 = int(w / 4)
    sh4 = 2 * fh4
    sw4 = 2 * fw4
    th4 = 3 * fh4
    tw4 = 3 * fw4
    hlist = [(0, fh4), (fh4, sh4), (sh4, th4), (th4, h)]
    wlist = [(0, fw4), (fw4, sw4), (sw4, tw4), (tw4, w)]
    blocks = []
    for (i, j) in hlist:
        for (k, l) in wlist:
            block = img[i:j, k:l]
            blocks.append(block)
    return blocks


def color_moments(image):
    moments = cv2.split(image)
    r, g, b = moments[:3]
    r_mean = np.mean(r)
    r_std = np.std(r)
    g_mean = np.mean(g)
    g_std = np.std(g)
    b_mean = np.mean(b)
    b_std = np.std(b)
    return [r_mean, r_std, g_mean, g_std, b_mean, b_std]


def extract_glcm(image):
    grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    glcm = greycomatrix(grey, [1], [0], 256)  # calculate glcm for one direction and 1 pixel offset
    contrast = greycoprops(glcm, 'contrast')[0][0]
    dissimilarity = greycoprops(glcm, 'dissimilarity')[0][0]
    homogeneity = greycoprops(glcm, 'homogeneity')[0][0]
    asm = greycoprops(glcm, 'ASM')[0][0]
    energy = greycoprops(glcm, 'energy')[0][0]
    texture_features_array = np.asarray(
        [contrast.astype(float), dissimilarity.astype(float), homogeneity, asm.astype(float), energy])
    normalized_texture_features_array = cv2.normalize(texture_features_array, texture_features_array)
    return fv2str(normalized_texture_features_array.tolist())


def extract_hu_moments(image):
    grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ret, th = cv2.threshold(grey, 127, 255, cv2.THRESH_BINARY)
    moments = cv2.HuMoments(cv2.moments(th)).flatten()
    return fv2str(moments.tolist())


def chi2_distance(animal, image, eps=1e-10):
    color_fv1 = str2fv(animal.color_fv)
    color_fv2 = str2fv(image.color_fv)
    color_d = 0.5 * np.sum([((a - b) ** 2) / (a + b + eps) for (a, b) in zip(color_fv1, color_fv2)])

    texture_fv1 = str2fv(animal.texture_fv)
    texture_fv2 = str2fv(image.texture_fv)
    texture_d = 0.5 * np.sum([((a - b) ** 2) / (a + b + eps) for (a, b) in zip(texture_fv1, texture_fv2)])

    shape_fv1 = str2fv(animal.shape_fv)
    shape_fv2 = str2fv(image.shape_fv)
    shape_d = 0.5 * np.sum([((a - b) ** 2) / (a + b + eps) for (a, b) in zip(shape_fv1, shape_fv2)])

    animal.distance = color_d
    return animal.distance


def fv2str(fv_as_list):
    return json.dumps(fv_as_list)


def str2fv(str):
    return json.loads(str)


def equalize(img):
    e = equalize_adapthist(img)
    result = img_as_ubyte(e)
    return result


class HeadDetector:
    def __init__(self):
        self.cascade = os.path.abspath(os.path.dirname("animals")) + "/animals/lbp_cat_face_detector.xml"
        self.detector = cv2.CascadeClassifier(self.cascade)
        self.scale_factor = 1.05
        self.min_neighbors = 5
        self.min_size= (30,30)

    def detect(self, image):
        loaded = self.detector.load(self.cascade)
        print(self.cascade)
        print(loaded)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        rects, rejectLevels, levelWeights = self.detector.detectMultiScale3(gray,scaleFactor=self.scale_factor,minNeighbors=self.min_neighbors,minSize=self.min_size,outputRejectLevels=True)
        if len(rects) > 0:
            return self.filter_rects(rects, levelWeights)
        else:
            return self.back_up_detect(gray)

    def back_up_detect(self, gray):
        rects, rejectLevels, levelWeights = self.detector.detectMultiScale3(gray,scaleFactor=self.scale_factor,minNeighbors=0,minSize=self.min_size,outputRejectLevels=True)
        if len(rects) > 0:
             return self.filter_rects(rects, levelWeights)
        else:
            return None

    def filter_rects(self, rects, level_weights):
        cat = None
        cat_weight = 0
        for i, r in enumerate(rects):
            if cat is None or cat_weight < level_weights[i]:
                cat = r
                cat_weight = level_weights[i]
        return cat