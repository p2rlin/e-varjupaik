# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

import PIL.Image as PImage
import numpy as np
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings
import sys
from animals.cbir import extract_color_moments, extract_glcm, extract_hu_moments, HeadDetector, equalize
import cv2

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=12)
    is_shelter = models.BooleanField(default=False)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class County(models.Model):
    name = models.CharField(max_length=100)


class Shelter(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='user')
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=12)
    email = models.CharField(max_length=100, default="")
    address = models.CharField(max_length=500)
    county = models.ForeignKey(County, on_delete=models.SET_NULL, null=True, default=14, related_name='county')
    image = models.ImageField(upload_to="profile_images",blank=True)
    description = models.CharField(max_length=10000)


class Species(models.Model):
    name = models.CharField(max_length=100)


class Age(models.Model):
    name = models.CharField(max_length=100)


class Procedure(models.Model):
    name = models.CharField(max_length=100)


class Gender(models.Model):
    name = models.CharField(max_length=100)


class Status(models.Model):
    name = models.CharField(max_length=100)


class Animal(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100)
    shelter = models.ForeignKey(Shelter, on_delete=models.SET_NULL, null=True, related_name='shelter')
    age = models.ForeignKey(Age, on_delete=models.SET_NULL, null=True, related_name='age')
    gender = models.ForeignKey(Gender, on_delete=models.SET_NULL, null=True, related_name='gender')
    species = models.ForeignKey(Species, on_delete=models.SET_NULL, null=True, related_name='species')
    description = models.TextField()
    sheltered_since = models.DateField(default=datetime.now, blank=True)
    status = models.ForeignKey(Status, on_delete=models.SET_DEFAULT, default=1,related_name='status')
    county = models.ForeignKey(County, on_delete=models.SET_NULL, null=True, default=14, related_name='location',blank=True)


class Image(models.Model):
    animal = models.ForeignKey(Animal, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField(upload_to="animal_images")
    is_profile_image = models.BooleanField(default=False)
    color_fv = models.CharField(max_length=10000, blank=True)
    texture_fv = models.CharField(max_length=10000, blank=True)
    shape_fv = models.CharField(max_length=10000, blank=True)
    distance = sys.maxsize

    def save(self, *args, **kwargs):
        image = PImage.open(self.image)
        image = np.asarray(image)
        image = equalize(image)

        if self.animal.species_id == 1:
            detector = HeadDetector()
            roi = detector.detect(image)
            face = image[roi[1]:roi[1]+roi[3], roi[0]:roi[0]+roi[2]]

            cv2.imwrite("face.png", face)
        else:
            face = image

        self.color_fv = extract_color_moments(face)
        self.texture_fv = extract_glcm(face)
        self.shape_fv = extract_hu_moments(face)
        super(Image, self).save(*args, **kwargs)


class ProceduresDone(models.Model):
    animal = models.ForeignKey(Animal, related_name='procedures', on_delete=models.CASCADE)
    procedure = models.ForeignKey(Procedure, on_delete=models.SET_NULL, null=True)


class SearchImage(models.Model):
    image = models.ImageField(upload_to="search_images")
    color_fv = models.CharField(max_length=40000, blank=True)
    texture_fv = models.CharField(max_length=40000, blank=True)
    shape_fv = models.CharField(max_length=40000, blank=True)
    species = models.ForeignKey(Species, on_delete=models.SET_NULL, null=True, related_name='search_species')

    def save(self, *args, **kwargs):
        image = PImage.open(self.image)
        image = np.asarray(image)
        image = equalize(image)

        if self.species_id == 1:
            detector = HeadDetector()
            roi = detector.detect(image)
            face = image[roi[1]:roi[1]+roi[3], roi[0]:roi[0]+roi[2]]

            cv2.imwrite("face.png", face)
        else:
            face = image

        self.color_fv = extract_color_moments(face)
        self.texture_fv = extract_glcm(face)
        self.shape_fv = extract_hu_moments(face)
        super(SearchImage, self).save(*args, **kwargs)


class Likes(models.Model):
    animal = models.ForeignKey(Animal, on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='likes', on_delete=models.CASCADE)
    like_time = models.DateTimeField(auto_now_add=True)
