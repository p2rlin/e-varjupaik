# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-28 16:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('animals', '0007_auto_20170428_1557'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='image',
            name='roi',
        ),
        migrations.AddField(
            model_name='image',
            name='roi_h',
            field=models.IntegerField(blank=True, default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='image',
            name='roi_w',
            field=models.IntegerField(blank=True, default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='image',
            name='roi_x',
            field=models.IntegerField(blank=True, default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='image',
            name='roi_y',
            field=models.IntegerField(blank=True, default=0),
            preserve_default=False,
        ),
    ]
