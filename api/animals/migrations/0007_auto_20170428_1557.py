# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-04-28 15:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('animals', '0006_auto_20170428_1548'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='roi',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='animal',
            name='name',
            field=models.CharField(max_length=350),
        ),
    ]
