from django.conf.urls import url, include
from animals import views as local_views
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views as rest_framework_views

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'profiles', local_views.ProfileViewSet)
router.register(r'shelters', local_views.SheltersViewSet)
router.register(r'ages', local_views.AgeViewSet)
router.register(r'genders', local_views.GenderViewSet)
router.register(r'species', local_views.SpeciesViewSet)
router.register(r'procedures', local_views.ProcedureViewSet)
router.register(r'status', local_views.StatusViewSet)
router.register(r'procedures_done', local_views.ProceduresDoneViewSet)
router.register(r'similar', local_views.SimilarImageViewSet)
router.register(r'animals', local_views.AnimalViewSet)
router.register(r'images', local_views.ImageViewSet)
router.register(r'users', local_views.UserViewSet)
router.register(r'likes', local_views.LikesViewSet)
router.register(r'counties', local_views.CountyViewSet)
router.register(r'search_images', local_views.SearchImageViewSet)

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^get_auth_token/$', rest_framework_views.obtain_auth_token, name='get_auth_token'),
]