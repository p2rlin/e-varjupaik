# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.reverse import reverse

from animals.models import Animal, Image, Age, Gender, ProceduresDone, Procedure, Species, SearchImage, Profile, \
    Shelter, Status, Likes, County
from animals.serializers import AnimalSerializer, ImageSerializer, AgeSerializer, GenderSerializer, \
    SpeciesSerializer, ProcedureSerializer, ProceduresDoneSerializer, AddAnimalSerializer, AddProceduresDoneSerializer,\
    AddImageSerializer, UserSerializer, SimilarImageSerializer, SearchImageSerializer, ProfileSerializer, \
    ShelterSerializer, StatusSerializer, LikeSerializer, CountySerializer, EditAnimalSerializer
from animals.cbir import chi2_distance

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'animals': reverse('animal-list', request=request, format=format)
    })


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    def get_queryset(self):
        queryset = Profile.objects.all()
        username = self.request.query_params.get('username', None)
        if username is not None:
            queryset = queryset.filter(user__username=username)
        return queryset


class SheltersViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Shelter.objects.all()
    serializer_class = ShelterSerializer

    def get_queryset(self):
        queryset = Shelter.objects.all()
        username = self.request.query_params.get('username', None)
        name = self.request.query_params.get('name', None)
        if name is not None:
            queryset = queryset.filter(name=name)
        if username is not None:
            queryset = queryset.filter(user__username=username)
        return queryset


class CountyViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = County.objects.all()
    serializer_class = CountySerializer


class AgeViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Age.objects.all()
    serializer_class = AgeSerializer


class GenderViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Gender.objects.all()
    serializer_class = GenderSerializer


class SpeciesViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Species.objects.all()
    serializer_class = SpeciesSerializer


class ProcedureViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Procedure.objects.all()
    serializer_class = ProcedureSerializer


class StatusViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Status.objects.all()
    serializer_class = StatusSerializer


class ProceduresDoneViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = ProceduresDone.objects.all()

    def get_serializer_class(self):
        if self.request.method in ('GET',):
            return ProceduresDoneSerializer
        return AddProceduresDoneSerializer

    def get_queryset(self):
        queryset = ProceduresDone.objects.all()
        animal = self.request.query_params.get('animal', None)
        procedure = self.request.query_params.get('procedure', None)
        if animal is not None:
            queryset = queryset.filter(animal__name=animal)
        elif procedure is not None:
            queryset = queryset.filter(procedure=procedure)
        return queryset


class AnimalViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Animal.objects.all()

    def get_serializer_class(self):
        if self.request.query_params.get('edit', None) is not None:
            return EditAnimalSerializer
        if self.request.method in ('GET',):
            return AnimalSerializer
        return AddAnimalSerializer

    def get_queryset(self):
        queryset = Animal.objects.all()
        animal = self.request.query_params.get('animal', None)
        order = self.request.query_params.get('order', None)
        limit = self.request.query_params.get('limit', None)
        shelter = self.request.query_params.get('shelter', None)
        likes = self.request.query_params.get('likes',None)
        my_animals = self.request.query_params.get('my_animals', None)
        if my_animals is not None:
            queryset = queryset.filter(shelter__user__username=self.request.query_params.get('user', None))
        if likes is not None:
            queryset = queryset.filter(likes__user__username=self.request.query_params.get('user', None))
        if shelter is not None:
            queryset = queryset.filter(status_id__in=[1, 2])
            queryset = queryset.filter(shelter__name=shelter)
        if order is not None:
            queryset = queryset.filter(status_id__in=[1, 2])
            queryset = queryset.order_by('-sheltered_since')
        if animal is not None:
            queryset = queryset.filter(animal__name=animal)
        if limit is not None:
            queryset = queryset.filter(status_id__in=[1, 2])
            if len(queryset) > 4:
                queryset = queryset[:4]
        return queryset

    def get_serializer_context(self):
        return {'user': self.request.query_params.get('user', None), 'request': self.request}


class LikesViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Likes.objects.all()
    serializer_class = LikeSerializer

    def get_queryset(self):
        queryset = Likes.objects.all()
        animal = self.request.query_params.get('animal', None)
        if animal is not None:
            queryset = queryset.filter(animal__id=animal)
        return queryset


class ImageViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Image.objects.all()

    def get_serializer_class(self):
        if self.request.method in ('GET',):
            return ImageSerializer
        return AddImageSerializer

    def get_queryset(self):
        queryset = Image.objects.all()
        animal = self.request.query_params.get('animal', None)
        if animal is not None:
            queryset = queryset.filter(animal__name=animal)
        return queryset


class SimilarImageViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Image.objects.all()
    serializer_class = SimilarImageSerializer

    def get_queryset(self):
        queryset = Image.objects.all().filter(is_profile_image=True)
        search_image_id = self.request.query_params.get('search_image', None)
        species_id = self.request.query_params.get('species', None)
        gender_id = self.request.query_params.get('gender', None)
        age_id = self.request.query_params.get('age', None)
        procedures = self.request.query_params.get('procedures', None)
        location = self.request.query_params.get('location', None)
        sheltered_since = self.request.query_params.get('sheltered_since', None)

        if age_id is not None:
            queryset = queryset.filter(animal__age=age_id)
        if species_id is not None:
            queryset = queryset.filter(animal__species=species_id)
        if gender_id is not None:
            queryset = queryset.filter(animal__gender=gender_id)
        if procedures is not None:
            procedure_list = procedures.split(",")
            for p in procedure_list:
                queryset = queryset.filter(animal__procedures__procedure_id=int(p))
        if location is not None:
            queryset = queryset.filter(animal__shelter__county_id=location)
        if sheltered_since is not None:
            queryset = queryset.filter(animal__sheltered_since__gt=sheltered_since)
        if search_image_id is not None:
            return self.get_similar_images(search_image_id, queryset)
        return queryset

    def get_similar_images(self, search_image_id, queryset):
        search_image = SearchImage.objects.get(pk=search_image_id)
        for i in queryset:
            chi2_distance(i, search_image)
        return sorted(queryset, key=lambda image: image.distance)

    def get_serializer_context(self):
        return {'user': self.request.query_params.get('user', None), 'request': self.request}


class SearchImageViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = SearchImage.objects.all()
    serializer_class = SearchImageSerializer
