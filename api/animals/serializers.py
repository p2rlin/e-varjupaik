from django.contrib.auth.models import User
from rest_framework import serializers

from animals.models import Animal, Image, Species, Age, Gender, Procedure, ProceduresDone, SearchImage, Profile, \
    Shelter, Status, Likes, County

class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12] # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension

        return extension


class CountySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = County
        fields = ('id','url','name')


class ShelterSerializer(serializers.HyperlinkedModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True,
    )
    county_name = serializers.ReadOnlyField(source='county.name', read_only=True)

    class Meta:
        model = Shelter
        fields = ('url','user','name','phone','email','address','county_name','image','description')


class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    email = serializers.ReadOnlyField(source='user.username', read_only=True)

    class Meta:
        model = Profile
        fields = ('url','name','phone','id','email','is_shelter','user')


class AgeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Age
        fields = ('name','url','id')


class GenderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Gender
        fields = ('name', 'url','id')


class SpeciesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Species
        fields = ('name', 'url', 'id')


class ProcedureSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Procedure
        fields = ('name','url','id')


class StatusSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Status
        fields = ('name','url','id')


class ProceduresDoneSerializer(serializers.HyperlinkedModelSerializer):
    procedure_name = serializers.ReadOnlyField(source='procedure.name', read_only=True)

    class Meta:
        model = ProceduresDone
        fields = ('procedure_name',)


class EditProcedureDoneSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProceduresDone
        fields = ('url','procedure')


class AddProceduresDoneSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProceduresDone
        fields = ('url', 'animal', 'procedure')


class ImageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Image
        fields = ('image', 'is_profile_image', 'url')


class AddImageSerializer(serializers.HyperlinkedModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True,
    )
    class Meta:
        model = Image
        fields = ('animal', 'image', 'is_profile_image')


class AnimalSerializer(serializers.HyperlinkedModelSerializer):
    images = ImageSerializer(Image.objects.all(), many=True, read_only=True)
    procedures = ProceduresDoneSerializer(ProceduresDone.objects.all(), many=True, read_only=True)
    shelter = serializers.ReadOnlyField(source='shelter.name', read_only=True)
    age = serializers.ReadOnlyField(source='age.name', read_only=True)
    gender = serializers.ReadOnlyField(source='gender.name', read_only=True)
    species = serializers.ReadOnlyField(source='species.name', read_only=True)
    status = serializers.ReadOnlyField(source='status.name', read_only=True)
    county = serializers.ReadOnlyField(source='county.name', read_only=True)
    is_my_animal = serializers.SerializerMethodField()
    like = serializers.SerializerMethodField()

    class Meta:
        model = Animal
        fields = ('url', 'id', 'name', 'shelter', 'age', 'gender', 'species', 'status', 'sheltered_since',
                  'description', 'images', 'procedures','is_my_animal','like','county')

    def get_is_my_animal(self,obj):
        user = self.context['user']
        if user is None:
            user = self.context['request'].user
            return user.username == obj.shelter.user.username
        return user == obj.shelter.user.username

    def get_like(self, obj):
        likes = Likes.objects.all()
        user = self.context['user']
        if user is None:
            user = self.context['request'].user
        likes = likes.filter(animal=obj, user__username=user)
        if len(likes) > 0:
            return likes[0].id
        return ""


class AddAnimalSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Animal
        fields = ('url', 'id', 'name', 'shelter', 'age', 'gender', 'species', 'sheltered_since', 'description', 'status','county')


class EditAnimalSerializer(serializers.HyperlinkedModelSerializer):
    images = ImageSerializer(Image.objects.all(), many=True, read_only=True)
    procedures = EditProcedureDoneSerializer(ProceduresDone.objects.all(), many=True, read_only=True)

    class Meta:
        model = Animal
        fields = ('url','id','name','shelter','age','gender','species','sheltered_since','description','images','procedures','status','county')


class LikeSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.ReadOnlyField(source='animal.name', read_only=True)
    shelter = serializers.ReadOnlyField(source='animal.shelter.name', read_only=True)
    image = serializers.SerializerMethodField()

    def get_image(self,obj):
        image = Image.objects.filter(is_profile_image=True,animal=obj.animal)
        return "http://127.0.0.1:8000" + image[0].image.url

    class Meta:
        model = Likes
        fields = ('url','animal','user','name','shelter','image')


class SimilarImageSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.ReadOnlyField(source='animal.name', read_only=True)
    shelter = serializers.ReadOnlyField(source='animal.shelter.name', read_only=True)
    id = serializers.ReadOnlyField(source='animal.id', read_only=True)
    is_my_animal = serializers.SerializerMethodField()
    like = serializers.SerializerMethodField()

    class Meta:
        model = Image
        fields = ('image', 'name', 'animal', 'shelter', 'id', 'distance', 'is_my_animal','like')

    def get_is_my_animal(self,obj):
        user = self.context['user']
        if user is None:
            user = self.context['request'].user
            return user.username == obj.animal.shelter.user.username
        return user == obj.animal.shelter.user.username

    def get_like(self, obj):
        likes = Likes.objects.all()
        user = self.context['user']
        if user is None:
            user = self.context['request'].user
        likes = likes.filter(animal=obj.animal, user__username=user)
        if len(likes) > 0:
            return likes[0].id
        return ""


class SearchImageSerializer(serializers.HyperlinkedModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True,
    )

    class Meta:
        model = SearchImage
        fields = ('image', 'id', 'species')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password')

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance