import Vue from 'vue';
import Router from 'vue-router';
import Home from './components/Home.vue';
import AnimalProfile from './components/AnimalProfile.vue';
import AddAnimal from './components/AddAnimal.vue';
import SearchSimilarAnimal from './components/SearchSimilarAnimal.vue';
import Shelters from './components/Shelters.vue';
import Shelter from './components/Shelter.vue';
import Login from './components/Login.vue';
import Register from './components/Register.vue';
import UserProfile from './components/UserProfile.vue';
import Editanimal from './components/EditAnimal.vue';
import LikedAnimals from './components/LikedAnimals.vue';
import MyAnimals from './components/MyAnimals.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/animals/:id',
      name: 'AnimalProfile',
      component: AnimalProfile
    },
    {
      path: '/edit/:id',
      name: 'EditAnimal',
      component: Editanimal
    },
    {
      path: '/add',
      name: 'AddAnimal',
      component: AddAnimal
    },
    {
      path: '/search_similar',
      name: 'SearchSimilarAnimal',
      component: SearchSimilarAnimal
    },
    {
      path: '/shelters',
      name: 'Shelters',
      component: Shelters
    },
    {
      path: '/shelters/:name',
      name: 'Shelter',
      component: Shelter
    },
    {
      path: '/myprofile',
      name: 'UserProfile',
      component: UserProfile
    },
    {
      path: '/likes',
      name: 'LikedAnimals',
      component: LikedAnimals
    },
    {
      path: '/myanimals',
      name: 'MyAnimals',
      component: MyAnimals
    }
  ]
});

