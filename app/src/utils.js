
class Utils {

  static getUser(store, query) {
    let user = "";
    if (store.state.token === "") {
      store.commit('login');
    }
    if (store.state.token !== "") {
      if (query) {
        user = "?user=" + store.state.username;
      } else {
        user = "&user=" + store.state.username;
      }
    }
    return user;
  }

  static isLoggedIn(store) {
    let isLoggedIn = false;
    if (store.state.token === "") {
      store.commit('login');
    }
    if (store.state.token !== "") {
      isLoggedIn = store.state.isLoggedIn;
    }
    return isLoggedIn;
  }

  static getProfileImage(images) {
    for (let i = 0; i < images.length; i++) {
      if (images[i].is_profile_image === true) {
        return images[i].image;
      }
    }
  }

  static getAuthHeader(store) {
    return {headers: {'Authorization': 'Token ' + store.state.token}}
  }

  static checkDate(date) {
    let dateRegex = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;
    return dateRegex.test(date);
  }

  static getImageName(name) {
    if (name.length <= 18) return name;
    return name.substr(0, 15) + '...';
  }

  static addLink() {
    let text = window.getSelection().getRangeAt(0);
    let a = document.createElement('a');
    a.href = prompt('Enter a URL:', '');
    text.surroundContents(a);
  }

  static paste(e) {
    e.preventDefault();
    let text = e.clipboardData.getData("text/plain");
    document.execCommand("insertHTML", false, text);
  }
}

export default Utils;