import Vue from 'vue';
import Vuex from 'vuex';
import VueResource from 'vue-resource';
import router from './router';
import App from './App.vue';
import AnimalCard from './components/AddAnimal.vue';

Vue.use(Vuex);
Vue.use(VueResource);

const store = new Vuex.Store({
  state: {
    userUrl: "",
    username: "",
    token: "",
    isLoggedIn: false,
    isShelter: false,
    apiUrl: "http://52.57.167.56:3000/"
  },
  mutations: {
    login (state) {
      let cookies = document.cookie.split("; ");
      for (let i = 0; i < cookies.length; i++) {
        let cookie = cookies[i].split("=");
        if (cookie[0] === "token") {
          state.token = cookie[1];
          state.isLoggedIn = true;
        } else if (cookie[0] === "username") {
          state.username = cookie[1];
        } else if (cookie[0] === "isShelter") {
          state.isShelter = true;
        } else if (cookie[0] === "userUrl") {
          state.userUrl = cookie[1];
        }
      }
    },
    logout(state) {
      document.cookie = 'token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
      document.cookie = 'username=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
      document.cookie = 'isShelter=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
      document.cookie = 'userUrl=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
      state.token = "";
      state.username = "";
      state.usernUrl = "";
      state.isLoggedIn = false;
      state.isShelter = false;
    },
    isShelter(state) {
      state.isShelter = true;
      document.cookie = 'isShelter=true';
    }
  }
});

new Vue({
  el: '#app',
  components: {
    'animal-card': AnimalCard
  },
  router,
  store,
  render: h => h(App),
  http: {
    emulateJSON: true,
    emulateHTTP: true
  }
});
